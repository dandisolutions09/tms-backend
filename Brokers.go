package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	// "time"

	//"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

type Broker struct {
	ID             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	FullName       string             `json:"full_name" bson:"full_name"`
	Address        string             `json:"address" bson:"address"`
	PhoneNumber    string             `json:"phone" bson:"phone"`
	Email          string             `json:"email" bson:"email"`
	City           string             `json:"city" bson:"city"`
	State          string             `json:"state" bson:"state"`
	Zip            string             `json:"zip" bson:"zip"`
	BillingState   string             `json:"billing_state" bson:"billing_state"`
	BillingCity    string             `json:"billing_city" bson:"billing_city"`
	BillingZipCode string             `json:"billing_zip_code" bson:"billing_zip_code"`
	BillingAddress string             `json:"billing_address" bson:"billing_address"`
}

func handleAddBroker(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	// fmt.Print(r.Body)

	decoder := json.NewDecoder(r.Body)
	// fmt.Printf(decoder)
	var data Broker
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(BrokerCollection)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting Broker", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	//fmt.Fprintf(w, "Broker inserted Successfully")

	response := map[string]string{"message": "Broker inserted Successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//GET Broker BY ID

func handleGetBrokerByID(w http.ResponseWriter, r *http.Request) {

	fmt.Print("get Broker by ID has been called....")
	// Get the broker ID from the request parameters
	params := mux.Vars(r)
	brokerID := params["id"]

	// Convert the Broker ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(brokerID)
	if err != nil {
		http.Error(w, "Invalid Broker ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific broker from MongoDB
	collection := client.Database(dbName).Collection(BrokerCollection)
	var broker Broker
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&broker)
	if err != nil {
		//http.Error(w, "Broker not found", http.StatusNotFound)
		response := map[string]string{"error": "Broker not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved Broker in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(broker)
}

//GET ALL Brokers

func handleGetBrokers(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(BrokerCollection)

	// Define a slice to store retrieved facilities
	var brokers []Broker

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving brokers", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var broker Broker
		if err := cursor.Decode(&broker); err != nil {
			http.Error(w, "Error decoding broker", http.StatusInternalServerError)
			return
		}
		brokers = append(brokers, broker)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(brokers)
}

//GET FACILITY BY ID

//
//UPDATE FACILITY

func handleUpdateBroker(w http.ResponseWriter, r *http.Request) {
	// Get the driver ID from the request parameters
	params := mux.Vars(r)
	brokerID := params["id"]

	// Convert the broker ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(brokerID)
	if err != nil {
		http.Error(w, "Invalid broker ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Broker
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Update the specific broker in MongoDB
	collection := client.Database(dbName).Collection(BrokerCollection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updatedData}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating broker", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	//fmt.Fprintf(w, "Driver updated successfully")

	response := map[string]string{"message": "Broker updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// DELETE Driver
func handleDeleteBroker(w http.ResponseWriter, r *http.Request) {
	// Get the broker ID from the request parameters
	params := mux.Vars(r)
	brokerID := params["id"]

	// Convert the broker ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(brokerID)
	if err != nil {
		//http.Error(w, "Invalid broker ID", http.StatusBadRequest)
		response := map[string]string{"error": "Invalid broker ID"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Delete the specific broker in MongoDB
	collection := client.Database(dbName).Collection(BrokerCollection)
	filter := bson.M{"_id": objID}

	res, err := collection.DeleteOne(context.Background(), filter)

	if err != nil {
		http.Error(w, "Error deleting broker", http.StatusInternalServerError)
		return
	}

	// Check if the document was found and deleted
	if res.DeletedCount == 0 {
		// If no documents were deleted, it might mean the document with the provided ID doesn't exist
		//http.NotFound(w, r)

		response := map[string]string{"error": "Broker not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

		return
	}

	// Respond with a JSON success message
	response := map[string]string{"message": "Broker deleted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
