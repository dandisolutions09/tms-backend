package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	// "time"

	//"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

type Driver struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	FirstName   string             `json:"first_name" bson:"first_name"`
	LastName    string             `json:"last_name" bson:"last_name"`
	Email       string             `json:"email" bson:"email"`
	City        string             `json:"city" bson:"city"`
	State       string             `json:"state" bson:"state"`
	Zip         string             `json:"zip" bson:"zip"`
	DateOfBirth string             `json:"date_of_birth" bson:"date_of_birth"`
	PhoneNumber string             `json:"phone" bson:"phone"`
	LicenseNo   string             `json:"linscense_no" bson:"linscense_no"`
}

func handleAddDriver(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	// fmt.Print(r.Body)

	decoder := json.NewDecoder(r.Body)
	// fmt.Printf(decoder)
	var data Driver
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(DriverCollection)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting Driver", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	//fmt.Fprintf(w, "Driver inserted Successfully")

	response := map[string]string{"message": "Driver inserted Successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//GET Driver BY ID

func handleGetDriverByID(w http.ResponseWriter, r *http.Request) {

	fmt.Print("get driver by ID has been called....")
	// Get the driver ID from the request parameters
	params := mux.Vars(r)
	driverID := params["id"]

	// Convert the Driver ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(driverID)
	if err != nil {
		http.Error(w, "Invalid Driver ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific driver from MongoDB
	collection := client.Database(dbName).Collection(DriverCollection)
	var driver Driver
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&driver)
	if err != nil {
		//http.Error(w, "Driver not found", http.StatusNotFound)
		response := map[string]string{"error": "Facility not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved driver in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(driver)
}

//GET ALL Drivers

func handleGetDrivers(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(DriverCollection)

	// Define a slice to store retrieved facilities
	var drivers []Driver

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving drivers", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var driver Driver
		if err := cursor.Decode(&driver); err != nil {
			http.Error(w, "Error decoding driver", http.StatusInternalServerError)
			return
		}
		drivers = append(drivers, driver)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(drivers)
}

//GET FACILITY BY ID

//
//UPDATE FACILITY

func handleUpdateDriver(w http.ResponseWriter, r *http.Request) {
	// Get the driver ID from the request parameters
	params := mux.Vars(r)
	driverID := params["id"]

	// Convert the driver ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(driverID)
	if err != nil {
		http.Error(w, "Invalid driver ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Driver
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Update the specific driver in MongoDB
	collection := client.Database(dbName).Collection(DriverCollection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updatedData}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating driver", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	//fmt.Fprintf(w, "Driver updated successfully")

	response := map[string]string{"message": "Driver updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// DELETE Driver
func handleDeleteDriver(w http.ResponseWriter, r *http.Request) {
	// Get the driver ID from the request parameters
	params := mux.Vars(r)
	driverID := params["id"]

	// Convert the driver ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(driverID)
	if err != nil {
		//http.Error(w, "Invalid driver ID", http.StatusBadRequest)
		response := map[string]string{"error": "Invalid driver ID"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Delete the specific driver in MongoDB
	collection := client.Database(dbName).Collection(DriverCollection)
	filter := bson.M{"_id": objID}

	res, err := collection.DeleteOne(context.Background(), filter)

	//result, err = collection.DeleteOne(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error deleting driver", http.StatusInternalServerError)
		return
	}

	// Check if the document was found and deleted
	if res.DeletedCount == 0 {
		// If no documents were deleted, it might mean the document with the provided ID doesn't exist
		//http.NotFound(w, r)

		response := map[string]string{"error": "Driver not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

		return
	}

	// Respond with a JSON success message
	response := map[string]string{"message": "Driver deleted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
