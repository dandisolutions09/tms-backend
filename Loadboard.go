package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	// "time"

	//"github.com/gorilla/handlers"
	// "github.com/gorilla/mux"
	// "go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

// type LoadBoard struct {
// 	ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
// 	FacilityName string             `json:"facility_name" bson:"facility_name"`
// 	EmailAddr    string             `json:"email_addr" bson:"email_addr"`
// 	City         string             `json:"city" bson:"city"`
// 	State        string             `json:"state" bson:"state"`
// 	Zip          string             `json:"zip" bson:"zip"`
// 	PhoneNumber  string             `json:"phone_number" bson:"phone_number"`
// }

// type LoadBoard struct {
// 	ID              primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
// 	PickUpFacility  string             `json:"pickup_facility" bson:"pickup_facility"`
// 	DropFacility    string             `json:"drop_facility" bson:"drop_facility"`
// 	Driver          string             `json:"driver" bson:"driver"`
// 	Broker          string             `json:"broker" bson:"broker"`
// 	Commodity       string             `json:"commodity" bson:"commodity"`
// 	ReferenceNumber string             `json:"reference_number" bson:"reference_number"`
// 	LoadRate        string             `json:"zip" bson:"zip"`
// 	Ratecon         string             `json:"phone_number" bson:"phone_number"`
// 	TruckNumber     string             `json:"truck_number" bson:"truck_number"`
// 	TrailerNumber   string             `json:"trailer_number" bson:"trailer_number"`
// }

type LoadSimple struct {
	ID              primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Shipper         shipper            `json:"shipper" bson:"shipper"`
	Receiver        receiver           `json:"receiver" bson:"receiver"`
	Driver          string             `json:"driver" bson:"driver"`
	Broker          string             `json:"broker" bson:"broker"`
	Commodity       string             `json:"commodity" bson:"commodity"`
	ReferenceNumber string             `json:"reference_number" bson:"reference_number"`
	LoadRate        string             `json:"zip" bson:"zip"`
	Ratecon         string             `json:"phone_number" bson:"phone_number"`
	TruckNumber     string             `json:"truck_number" bson:"truck_number"`
	TrailerNumber   string             `json:"trailer_number" bson:"trailer_number"`
}

type LoadMultiPickandDrop struct {
	ID              primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Shipper         []shipper          `json:"shipper" bson:"shipper"`
	Receiver        []receiver         `json:"receiver" bson:"receiver"`
	Driver          string             `json:"driver" bson:"driver"`
	Broker          string             `json:"broker" bson:"broker"`
	Commodity       string             `json:"commodity" bson:"commodity"`
	ReferenceNumber string             `json:"reference_number" bson:"reference_number"`
	LoadRate        string             `json:"zip" bson:"zip"`
	Ratecon         string             `json:"phone_number" bson:"phone_number"`
	TruckNumber     string             `json:"truck_number" bson:"truck_number"`
	TrailerNumber   string             `json:"trailer_number" bson:"trailer_number"`
}

type splitLoad struct {
	ID       primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Shipper  []shipper          `json:"shipper" bson:"shipper"`
	Waypoint []waypointType

	Receiver        []receiver `json:"receiver" bson:"receiver"`
	Driver          string     `json:"driver" bson:"driver"`
	Broker          string     `json:"broker" bson:"broker"`
	Commodity       string     `json:"commodity" bson:"commodity"`
	ReferenceNumber string     `json:"reference_number" bson:"reference_number"`
	LoadRate        string     `json:"zip" bson:"zip"`
	Ratecon         string     `json:"phone_number" bson:"phone_number"`
	TruckNumber     string     `json:"truck_number" bson:"truck_number"`
	TrailerNumber   string     `json:"trailer_number" bson:"trailer_number"`
}

type shipper struct {
	PickUpFacility facility `json:"pickup_facility" bson:"pickup_facility"`
	PickupDateTime DateTime `json:"date_time" bson:"date_time"`
	PickupNumber   string   `json:"pickup_number" bson:"pickup_number"`
	PhoneNumber    string   `json:"phone_number" bson:"phone_number"`
}

type DateTime struct {
	PickupTime string `json:"pickup_time" bson:"pickup_time"`
	PickupDate string `json:"pickup_date" bson:"pickup_date"`
}

type receiver struct {
	DropFacility   facility `json:"drop_facility" bson:"drop_facility"`
	DropDateTime   DateTime `json:"date_time" bson:"date_time"`
	DeliveryNumber string   `json:"delivery_number" bson:"delivery_number"`
	PhoneNumber    string   `json:"phone_number" bson:"phone_number"`
}

type waypointType struct {
	Driver driver   `json:"driver" bson:"driver"`
	Waypt  facility `json:"waypoint" bson:"waypoint"`
}
type driver struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	FirstName   string             `json:"first_name" bson:"first_name"`
	LastName    string             `json:"last_name" bson:"last_name"`
	Email       string             `json:"email" bson:"email"`
	City        string             `json:"city" bson:"city"`
	State       string             `json:"state" bson:"state"`
	Zip         string             `json:"zip" bson:"zip"`
	DateOfBirth string             `json:"date_of_birth" bson:"date_of_birth"`
	PhoneNumber string             `json:"phone" bson:"phone"`
	LicenseNo   string             `json:"linscense_no" bson:"linscense_no"`
}

type facility struct {
	//ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	FacilityName string `json:"facility_name" bson:"facility_name"`
	EmailAddr    string `json:"email_addr" bson:"email_addr"`
	City         string `json:"city" bson:"city"`
	State        string `json:"state" bson:"state"`
	Zip          string `json:"zip" bson:"zip"`
	PhoneNumber  string `json:"phone_number" bson:"phone_number"`
}

// type L2_pickup_facility struct {
// 	City    string `json:"city" bson:"city"`
// 	State   string `json:"state" bson:"state"`
// 	Country string `json:"country" bson:"country"`
// }

// type L2_drop_facility struct {
// 	City    string `json:"city" bson:"city"`
// 	State   string `json:"state" bson:"state"`
// 	Country string `json:"country" bson:"country"`
// }

func handleAddLoadSimple(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	// fmt.Print(r.Body)

	decoder := json.NewDecoder(r.Body)
	// fmt.Printf(decoder)
	var data LoadSimple
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(LoadBoardCollection)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting facility", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	fmt.Fprintf(w, "Facility inserted Successfully")

	response := map[string]string{"message": "Facility inserted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func handleAddLoadMulti(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	// fmt.Print(r.Body)

	decoder := json.NewDecoder(r.Body)
	// fmt.Printf(decoder)
	var data LoadMultiPickandDrop
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(LoadBoardCollection_multi)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting facility", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	fmt.Fprintf(w, "Facility inserted Successfully")

	response := map[string]string{"message": "Facility inserted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//GET FACILITY BY ID



// //GET ALL FACILITIES

func handleGetLoads(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(LoadBoardCollection)

	// Define a slice to store retrieved facilities
	var loads []LoadSimple

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving loads", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var load LoadSimple
		if err := cursor.Decode(&load); err != nil {
			http.Error(w, "Error decoding loads", http.StatusInternalServerError)
			return
		}
		loads = append(loads, load)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(loads)
}

// //GET FACILITY BY ID

// //
// //UPDATE FACILITY

// func handleUpdateFacility(w http.ResponseWriter, r *http.Request) {
// 	// Get the facility ID from the request parameters
// 	params := mux.Vars(r)
// 	facilityID := params["id"]

// 	// Convert the facility ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(facilityID)
// 	if err != nil {
// 		http.Error(w, "Invalid facility ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Parse JSON data from the request
// 	decoder := json.NewDecoder(r.Body)
// 	var updatedData Facility
// 	err = decoder.Decode(&updatedData)
// 	if err != nil {
// 		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
// 		return
// 	}

// 	// Update the specific facility in MongoDB
// 	collection := client.Database(dbName).Collection(FacilityCollection)
// 	filter := bson.M{"_id": objID}
// 	update := bson.M{"$set": updatedData}

// 	_, err = collection.UpdateOne(context.Background(), filter, update)
// 	if err != nil {
// 		http.Error(w, "Error updating facility", http.StatusInternalServerError)
// 		return
// 	}

// 	// Respond with a success message
// 	//fmt.Fprintf(w, "Facility updated successfully")

// 	response := map[string]string{"message": "Facility updated successfully"}
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(response)
// }

// // DELETE FACILITY
// func handleDeleteFacility(w http.ResponseWriter, r *http.Request) {
// 	// Get the facility ID from the request parameters
// 	params := mux.Vars(r)
// 	facilityID := params["id"]

// 	// Convert the facility ID to a MongoDB ObjectID
// 	objID, err := primitive.ObjectIDFromHex(facilityID)
// 	if err != nil {
// 		http.Error(w, "Invalid facility ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Delete the specific facility in MongoDB
// 	collection := client.Database(dbName).Collection(FacilityCollection)
// 	filter := bson.M{"_id": objID}

// 	res, err := collection.DeleteOne(context.Background(), filter)

// 	//result, err = collection.DeleteOne(context.Background(), filter)
// 	if err != nil {
// 		http.Error(w, "Error deleting driver", http.StatusInternalServerError)
// 		return
// 	}

// 	// Check if the document was found and deleted
// 	if res.DeletedCount == 0 {
// 		// If no documents were deleted, it might mean the document with the provided ID doesn't exist
// 		//http.NotFound(w, r)
// 		response := map[string]string{"error": "Facility not found"}
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(response)
// 		return
// 	}

// 	// Respond with a JSON success message
// 	response := map[string]string{"message": "Facility deleted successfully"}
// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(response)
// }
