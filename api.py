import requests

def get_facility_by_id(facility_id):
    url = f"http://localhost:8080/facility/{facility_id}"
    
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise an exception for 4xx and 5xx status codes
        data = response.json()
        return data
    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")
        return None

# Replace '123' with the actual facility ID you want to query
facility_id_to_query = '657ce65746d5a9b6e542e29a'
result = get_facility_by_id(facility_id_to_query)

if result is not None:
    print("Facility data:")
    print(result)
else:
    print("Failed to retrieve facility data.")
