package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	// "time"

	//"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

type Facility struct {
	ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	FacilityName string             `json:"facility_name" bson:"facility_name"`
	EmailAddr    string             `json:"email_addr" bson:"email_addr"`
	City         string             `json:"city" bson:"city"`
	State        string             `json:"state" bson:"state"`
	Zip          string             `json:"zip" bson:"zip"`
	PhoneNumber  string             `json:"phone_number" bson:"phone_number"`
}

func handleAddFacility(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	// fmt.Print(r.Body)

	decoder := json.NewDecoder(r.Body)
	// fmt.Printf(decoder)
	var data Facility
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(FacilityCollection)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting facility", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	fmt.Fprintf(w, "Facility inserted Successfully")

	response := map[string]string{"message": "Facility inserted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

//GET FACILITY BY ID

func handleGetFacilityByID(w http.ResponseWriter, r *http.Request) {

	fmt.Print("get facility by ID has been called....")
	// Get the facility ID from the request parameters
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid facility ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific facility from MongoDB
	collection := client.Database(dbName).Collection(FacilityCollection)
	var facility Facility
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&facility)
	if err != nil {
		//http.Error(w, "Facility not found", http.StatusNotFound)
		response := map[string]string{"error": "Facility not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved facility in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(facility)
}

//GET ALL FACILITIES

func handleGetFacilities(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(FacilityCollection)

	// Define a slice to store retrieved facilities
	var facilities []Facility

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving facilities", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var facility Facility
		if err := cursor.Decode(&facility); err != nil {
			http.Error(w, "Error decoding facility", http.StatusInternalServerError)
			return
		}
		facilities = append(facilities, facility)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(facilities)
}

//GET FACILITY BY ID

//
//UPDATE FACILITY

func handleUpdateFacility(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid facility ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Facility
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Update the specific facility in MongoDB
	collection := client.Database(dbName).Collection(FacilityCollection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updatedData}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating facility", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	//fmt.Fprintf(w, "Facility updated successfully")

	response := map[string]string{"message": "Facility updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// DELETE FACILITY
func handleDeleteFacility(w http.ResponseWriter, r *http.Request) {
	// Get the facility ID from the request parameters
	params := mux.Vars(r)
	facilityID := params["id"]

	// Convert the facility ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(facilityID)
	if err != nil {
		http.Error(w, "Invalid facility ID", http.StatusBadRequest)
		return
	}

	// Delete the specific facility in MongoDB
	collection := client.Database(dbName).Collection(FacilityCollection)
	filter := bson.M{"_id": objID}

	res, err := collection.DeleteOne(context.Background(), filter)

	//result, err = collection.DeleteOne(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error deleting driver", http.StatusInternalServerError)
		return
	}

	// Check if the document was found and deleted
	if res.DeletedCount == 0 {
		// If no documents were deleted, it might mean the document with the provided ID doesn't exist
		//http.NotFound(w, r)
		response := map[string]string{"error": "Facility not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with a JSON success message
	response := map[string]string{"message": "Facility deleted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
