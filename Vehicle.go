package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	// "time"

	//"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	//"go.mongodb.org/mongo-driver/mongo"
	//"go.mongodb.org/mongo-driver/mongo/options"
)

type Vehicle struct {
	ID             primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Model          string             `json:"model" bson:"model"`
	LicensePlate   string             `json:"license_plate" bson:"license_plate"`
	Vin            string             `json:"vin" bson:"vin"`
	GrossWeight    string             `json:"gross_weight" bson:"gross_weight"`
	Make           string             `json:"make" bson:"make"`
	Year           string             `json:"year" bson:"year"`
	LPExpiry       string             `json:"lp_expiry" bson:"lp_expiry"`
	LastInspection string             `json:"last_inspection" bson:"last_inspection"`
	MaxWeight      string             `json:"max_weight" bson:"max_weight"`
	CubicFt        string             `json:"cubic_ft" bson:"cubic_ft"`
	Type           string             `json:"type" bson:"type"`
}

func handleAddVehicle(w http.ResponseWriter, r *http.Request) {
	// Parse JSON data from the request
	// fmt.Print(r.Body)

	decoder := json.NewDecoder(r.Body)
	// fmt.Printf(decoder)
	var data Vehicle
	err := decoder.Decode(&data)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Insert data into MongoDB
	collection := client.Database(dbName).Collection(VehicleCollection)
	_, err = collection.InsertOne(context.Background(), data)
	fmt.Print(data)
	if err != nil {
		http.Error(w, "Error inserting Vehicle", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	//fmt.Fprintf(w, "Vehicle inserted Successfully")

	response := map[string]string{"message": "Vehicle inserted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}

//GET Vehicle BY ID

func handleGetVehicleByID(w http.ResponseWriter, r *http.Request) {

	fmt.Print("get vehicle by ID has been called....")
	// Get the vehicle ID from the request parameters
	params := mux.Vars(r)
	vehicleID := params["id"]

	// Convert the vehicle ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(vehicleID)
	if err != nil {
		http.Error(w, "Invalid vehicle ID", http.StatusBadRequest)
		return
	}

	// Retrieve the specific vehicle from MongoDB
	collection := client.Database(dbName).Collection(VehicleCollection)
	var vehicle Vehicle
	err = collection.FindOne(context.Background(), bson.M{"_id": objID}).Decode(&vehicle)
	if err != nil {
		//http.Error(w, "Vehicle not found", http.StatusNotFound)
		response := map[string]string{"error": "Vehicle not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Respond with the retrieved vehicle in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(vehicle)
}

//GET ALL Vehicles

func handleGetVehicles(w http.ResponseWriter, r *http.Request) {
	collection := client.Database(dbName).Collection(VehicleCollection)

	// Define a slice to store retrieved facilities
	var vehicles []Vehicle

	// Retrieve all documents from the collection
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		http.Error(w, "Error retrieving vehicles", http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.Background())

	// Iterate through the cursor and decode documents into the facilities slice
	for cursor.Next(context.Background()) {
		var vehicle Vehicle
		if err := cursor.Decode(&vehicle); err != nil {
			http.Error(w, "Error decoding vehicle", http.StatusInternalServerError)
			return
		}
		vehicles = append(vehicles, vehicle)
	}

	// Respond with the retrieved facilities in JSON format
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(vehicles)
}

//GET FACILITY BY ID

//
//UPDATE FACILITY

func handleUpdateVehicle(w http.ResponseWriter, r *http.Request) {
	// Get the vehicle ID from the request parameters
	params := mux.Vars(r)
	vehicleID := params["id"]

	// Convert the vehicle ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(vehicleID)
	if err != nil {
		http.Error(w, "Invalid vehicle ID", http.StatusBadRequest)
		return
	}

	// Parse JSON data from the request
	decoder := json.NewDecoder(r.Body)
	var updatedData Vehicle
	err = decoder.Decode(&updatedData)
	if err != nil {
		http.Error(w, "Error decoding JSON", http.StatusBadRequest)
		return
	}

	// Update the specific vehicle in MongoDB
	collection := client.Database(dbName).Collection(VehicleCollection)
	filter := bson.M{"_id": objID}
	update := bson.M{"$set": updatedData}

	_, err = collection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		http.Error(w, "Error updating vehicle", http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	//fmt.Fprintf(w, "Vehicle updated successfully")

	response := map[string]string{"message": "Vehicle updated successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// DELETE vehicle
func handleDeleteVehicle(w http.ResponseWriter, r *http.Request) {
	// Get the vehicle ID from the request parameters
	params := mux.Vars(r)
	vehicleID := params["id"]

	// Convert the vehicle ID to a MongoDB ObjectID
	objID, err := primitive.ObjectIDFromHex(vehicleID)
	if err != nil {
		//http.Error(w, "Invalid vehicle ID", http.StatusBadRequest)
		response := map[string]string{"error": "Invalid vehicle ID"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
		return
	}

	// Delete the specific vehicle in MongoDB
	collection := client.Database(dbName).Collection(VehicleCollection)
	filter := bson.M{"_id": objID}

	res, err := collection.DeleteOne(context.Background(), filter)

	//result, err = collection.DeleteOne(context.Background(), filter)
	if err != nil {
		http.Error(w, "Error deleting vehicle", http.StatusInternalServerError)
		return
	}

	// Check if the document was found and deleted
	if res.DeletedCount == 0 {
		// If no documents were deleted, it might mean the document with the provided ID doesn't exist
		//http.NotFound(w, r)

		response := map[string]string{"error": "Vehicle not found"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)

		return
	}

	// Respond with a JSON success message
	response := map[string]string{"message": "vehicle deleted successfully"}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
