package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var dbName = "tms_db"
var FacilityCollection = "Fac"
var DriverCollection = "Drivers"
var VehicleCollection = "Vehicles"
var BrokerCollection = "Brokers"
var LoadBoardCollection = "LoadBoard-simple"
var LoadBoardCollection_multi = "LoadBoard-multi"


var mongoURI = "mongodb+srv://dandisolutions09:GAFmLYi25DojAuM1@tms-cluster.cp6qfmq.mongodb.net/tms_db"

var client *mongo.Client

func init() {
	// Set up MongoDB client
	clientOptions := options.Client().ApplyURI(mongoURI)
	var err error
	client, err = mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		fmt.Println("Error connecting to MongoDB:", err)
		return
	}

	// Check the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		fmt.Println("Error pinging MongoDB:", err)
		return
	}

	fmt.Println("Connected to MongoDB")
}

// Define a struct for your data (adjust fields as needed)
func main() {
	// Create a new Gorilla Mux router
	router := mux.NewRouter()

	// Use the handlers.CORS middleware to handle CORS
	corsHandler := handlers.CORS(
		handlers.AllowedHeaders([]string{"Content-Type", "Authorization"}),
		handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}),
	)

	// Attach the CORS middleware to your router

	//LOADBOARD ROUTES
	//router.HandleFunc("/add-facility", handleAddLoad).Methods("POST")

	// //FACILITY ROUTES
	router.HandleFunc("/add-facility", handleAddFacility).Methods("POST")
	router.HandleFunc("/get-facilities", handleGetFacilities).Methods("GET")
	router.HandleFunc("/facility/{id}", handleGetFacilityByID).Methods("GET")
	router.HandleFunc("/edit-facility/{id}", handleUpdateFacility).Methods("PUT")
	router.HandleFunc("/delete-facility/{id}", handleDeleteFacility).Methods("DELETE")

	//DRIVER ROUTES
	router.HandleFunc("/add-driver", handleAddDriver).Methods("POST")
	router.HandleFunc("/get-drivers", handleGetDrivers).Methods("GET")
	router.HandleFunc("/driver/{id}", handleGetDriverByID).Methods("GET")
	router.HandleFunc("/edit-driver/{id}", handleUpdateDriver).Methods("PUT")
	router.HandleFunc("/delete-driver/{id}", handleDeleteDriver).Methods("DELETE")

	//VEHICLE ROUTES
	router.HandleFunc("/add-vehicle", handleAddVehicle).Methods("POST")
	router.HandleFunc("/get-vehicles", handleGetVehicles).Methods("GET")
	router.HandleFunc("/vehicle/{id}", handleGetVehicleByID).Methods("GET")
	router.HandleFunc("/edit-vehicle/{id}", handleUpdateVehicle).Methods("PUT")
	router.HandleFunc("/delete-vehicle/{id}", handleDeleteVehicle).Methods("DELETE")

	//BROKER ROUTES
	router.HandleFunc("/add-broker", handleAddBroker).Methods("POST")
	router.HandleFunc("/get-brokers", handleGetBrokers).Methods("GET")
	router.HandleFunc("/broker/{id}", handleGetBrokerByID).Methods("GET")
	router.HandleFunc("/edit-broker/{id}", handleUpdateBroker).Methods("PUT")
	router.HandleFunc("/delete-broker/{id}", handleDeleteBroker).Methods("DELETE")

	// router.HandleFunc("/get-vehicles", handleGetVehicles).Methods("GET")

	//LOADBOARD handleAddLoadSimple

	router.HandleFunc("/add-load-simple", handleAddLoadSimple).Methods("POST")
	router.HandleFunc("/add-load-multi", handleAddLoadMulti).Methods("POST")
	router.HandleFunc("/get-loads", handleGetLoads).Methods("GET")



	// Start the server on port 8080
	http.Handle("/", corsHandler(router))
	fmt.Println("Server listening on :8080")
	http.ListenAndServe(":8080", nil)
}
